package com.example.cinemamvvm.presentation.ViewModel

import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel
import com.example.cinemamvvm.network.Login
import com.example.cinemamvvm.network.MyRetrofit
import kotlinx.coroutines.CoroutineScope
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.launch
import retrofit2.Call
import retrofit2.Response
import java.lang.Exception

class SignInViewModel: ViewModel() {
    val errorTag = MutableLiveData<String>()
    val resultToken = MutableLiveData<String>()

    fun loginRequest(hashMap: HashMap<String, String>) {
//        call.enqueue(object : retrofit2.Callback<Login>{
//            override fun onResponse(call: Call<Login>, response: Response<Login>) {
//                if (response.isSuccessful) {
//                    resultToken.value = response.body()!!.token
//                } else {
//                    errorTag.value = response.body().toString()
//                }
//            }
//
//            override fun onFailure(call: Call<Login>, t: Throwable) {
//                errorTag.value = t.localizedMessage
//            }
//        })
        MyRetrofit.getRetrofit().loginRequest(hashMap).enqueue(object : retrofit2.Callback<Login>{
            override fun onResponse(call: Call<Login>, response: Response<Login>) {
                if (response.isSuccessful) {
                    resultToken.value = response.body()!!.token
                } else {
                    errorTag.value = "Ошибка ${response.body() ?: "null"}"
                }
            }

            override fun onFailure(call: Call<Login>, t: Throwable) {
                errorTag.value = "Произошла ошибка ${t.localizedMessage}"
            }
        })
    }


}