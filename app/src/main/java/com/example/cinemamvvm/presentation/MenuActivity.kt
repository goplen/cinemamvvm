package com.example.cinemamvvm.presentation

import android.os.Bundle
import android.widget.Toast
import com.google.android.material.bottomnavigation.BottomNavigationView
import androidx.appcompat.app.AppCompatActivity
import androidx.navigation.findNavController
import androidx.navigation.ui.setupWithNavController
import com.example.cinemamvvm.R
import com.example.cinemamvvm.RecyclerClick
import com.example.cinemamvvm.databinding.ActivityMenuBinding

class MenuActivity : AppCompatActivity(),RecyclerClick {

    private lateinit var binding: ActivityMenuBinding

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)

        binding = ActivityMenuBinding.inflate(layoutInflater)
        setContentView(binding.root)

        val navView: BottomNavigationView = binding.navView

        val navController = findNavController(R.id.nav_host_fragment_activity_menu)
        // Passing each menu ID as a set of Ids because each
        // menu should be considered as top level destinations.

        navView.setupWithNavController(navController)
    }

    override fun myClick(arg: String) {
        Toast.makeText(this, arg, Toast.LENGTH_SHORT).show()
    }
}