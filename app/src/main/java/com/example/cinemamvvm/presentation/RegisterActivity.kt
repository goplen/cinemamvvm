package com.example.cinemamvvm.presentation

import android.app.Activity
import android.app.AlertDialog
import android.content.Context
import android.content.Intent
import android.content.SharedPreferences
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.util.Log
import android.view.LayoutInflater
import android.widget.TextView
import androidx.lifecycle.ViewModelProvider
import com.example.cinemamvvm.R
import com.example.cinemamvvm.common.Utils
import com.example.cinemamvvm.databinding.ActivityRegisterBinding
import com.example.cinemamvvm.presentation.ViewModel.RegisterViewModel
import java.util.regex.Pattern

class RegisterActivity : AppCompatActivity() {

    private lateinit var binding: ActivityRegisterBinding
    private lateinit var vm: RegisterViewModel
    private lateinit var sharedPreferences: SharedPreferences
    private lateinit var editor: SharedPreferences.Editor

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        binding = ActivityRegisterBinding.inflate(layoutInflater)
        vm = ViewModelProvider(this).get(RegisterViewModel::class.java)
        sharedPreferences = getSharedPreferences("main", Context.MODE_PRIVATE)
        editor = sharedPreferences.edit()
        setContentView(binding.root)

        val layout = LayoutInflater.from(this).inflate(R.layout.recylcler_item_1, null)
        val text_btn = layout.findViewById<TextView>(R.id.text1)
        text_btn.setOnClickListener {
            Log.d("open", "wwww")
        }
        text_btn.text = "12031023"
        val alert = AlertDialog.Builder(this)
            .setView(layout)
            .setPositiveButton("ok", null)
            .create()

        binding.registerBtn.setOnClickListener {
            alert.show()
//            val name: String = binding.nameForm.text.toString()
//            val surname: String = binding.surnameForm.text.toString()
//            val email: String = binding.emailForm.text.toString()
//            val password: String = binding.passwordForm2.text.toString()
//            val password_retry: String = binding.passwordRetryForm.text.toString()
//            vm.authRequestValidate(name,surname,email,password,password_retry)
        }

        vm.authSuccess.observe(this) {
            editor.putString("token", it.toString())
            editor.apply()
            val intent = Intent(this, MenuActivity::class.java)
            startActivity(intent)
            finish()
        }

        vm.errorTag.observe(this) {
            Utils.alertDialog(this, it.toString())
        }

    }
}