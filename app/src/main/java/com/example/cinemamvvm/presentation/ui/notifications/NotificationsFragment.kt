package com.example.cinemamvvm.presentation.ui.notifications

import android.app.Activity
import android.content.Context
import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.TextView
import androidx.fragment.app.Fragment
import androidx.lifecycle.ViewModelProvider
import androidx.navigation.fragment.findNavController
import com.example.cinemamvvm.R
import com.example.cinemamvvm.RecyclerClick
import com.example.cinemamvvm.databinding.FragmentNotificationsBinding

class NotificationsFragment : Fragment() {


    var click: RecyclerClick? = null


    override fun onAttach(activity: Context) {
        super.onAttach(activity)
        click= context as RecyclerClick
    }

    private var _binding: FragmentNotificationsBinding? = null

    // This property is only valid between onCreateView and
    // onDestroyView.
    private val binding get() = _binding!!

    override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View {
        val notificationsViewModel =
            ViewModelProvider(this).get(NotificationsViewModel::class.java)

        _binding = FragmentNotificationsBinding.inflate(inflater, container, false)
        val root: View = binding.root
        click!!.myClick("123")
        val navController =findNavController()

        binding.button.setOnClickListener {
            navController.navigate(R.id.action_navigation_notifications_to_navigation_home)
        }
        val textView: TextView = binding.textNotifications
        notificationsViewModel.text.observe(viewLifecycleOwner) {
            textView.text = it
        }
        return root
    }

    override fun onDestroyView() {
        super.onDestroyView()
        _binding = null
    }
}