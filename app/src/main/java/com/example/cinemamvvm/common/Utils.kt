package com.example.cinemamvvm.common

import android.app.AlertDialog
import android.content.Context
import android.content.Intent
import com.example.cinemamvvm.presentation.MenuActivity


object Utils {
    fun baseUrl() = "http://cinema.areas.su/"
    fun alertDialog(context: Context, message: String) = AlertDialog.Builder(context)
        .setMessage(message)
        .setPositiveButton("Ok") { _, _ ->}
        .create()
        .show()
    fun EMAIL_PATTERN() = "[a-zA-Z]{1,50}" +
            "\\@" +
            "[a-zA-Z]{1,12}" +
            "\\." +
            "[a-zA-Z]{1,6}"
}