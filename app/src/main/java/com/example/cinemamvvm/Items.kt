package com.example.cinemamvvm

import java.util.*

data class Items(
    val text:String,
    val type: Type
)

enum class Type{
    Courier,
    Order
}

data class Courier(
    val courierId : Int,
    val name: String,
    val orders : List<Order>
)
data class Order(
    val address:String,
    val date: String
)

data class CourierForRecycler(
    val viewType: Type,
    val courierId : Int,
    val name: String?= null,
    val orders : Order?=null
)

fun Courier.toCourierForRecycler():CourierForRecycler{
    return CourierForRecycler(
        viewType = Type.Courier,
        name = this.name,
        courierId = this.courierId
    )
}


