package com.example.cinemamvvm

import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.TextView
import androidx.recyclerview.widget.RecyclerView

class MyRecycler(val list:List<CourierForRecycler>,val click:RecyclerClick): RecyclerView.Adapter<RecyclerView.ViewHolder>() {

    class ViewHolder1(itemView: View) : RecyclerView.ViewHolder(itemView){
        val text = itemView.findViewById<TextView>(R.id.text1)
    }


    class ViewHolder2(itemView: View) : RecyclerView.ViewHolder(itemView){
        val text = itemView.findViewById<TextView>(R.id.text2)
    }


    override fun getItemViewType(position: Int): Int {
        return when(list[position].viewType){
            Type.Courier -> 0
            Type.Order -> 1
        }

    }

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): RecyclerView.ViewHolder {
        return when(viewType){
            0-> ViewHolder1(LayoutInflater.from(parent.context).inflate(R.layout.recylcler_item_1,parent,false))
            1-> ViewHolder2(LayoutInflater.from(parent.context).inflate(R.layout.recylcler_item_2,parent,false))
            else -> ViewHolder1(LayoutInflater.from(parent.context).inflate(R.layout.recylcler_item_1,parent,false))
        }
    }

    override fun onBindViewHolder(holder: RecyclerView.ViewHolder, position: Int) {
        when(holder.itemViewType){
           0-> {
               holder as ViewHolder1
               holder.text.text = list[position].name
               holder.text.setOnClickListener {
                   click.myClick("qwe")
               }
           }
            1-> {
                holder as ViewHolder2
                holder.text.text = list[position].orders!!.address

            }
        }
    }

    override fun getItemCount(): Int {
        return list.size
    }

}