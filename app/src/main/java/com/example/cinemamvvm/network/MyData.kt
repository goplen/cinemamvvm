package com.example.cinemamvvm.network

data class Login(val token: String)
data class Cover(val movieId: String, val backgroundImage: String, val foregroundImage: String)
data class Movies(val movieId: String, val name: String, val description: String, val age: String, val images: String, val poster: String, val tags: List<Tags>)
data class Tags(val idTags: String, val tagName: String)
