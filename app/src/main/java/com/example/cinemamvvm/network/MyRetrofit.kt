package com.example.cinemamvvm.network

import com.example.cinemamvvm.common.Utils
import retrofit2.Retrofit
import retrofit2.converter.gson.GsonConverterFactory

object MyRetrofit {
    val retrofit = Retrofit.Builder()
        .baseUrl(Utils.baseUrl())
        .addConverterFactory(GsonConverterFactory.create())
        .build()
    fun getRetrofit() = retrofit.create(MyInterface::class.java)
}